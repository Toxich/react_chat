<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = ['name', 'is_active'];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
