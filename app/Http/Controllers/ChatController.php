<?php

namespace App\Http\Controllers;

use App\Chat;
use App\ChatUser;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Events\MessageSent;

class ChatController extends Controller
{
    public function index(Request $request)
    {

        $validatedData = $request->validate([
                                                'id' => 'required'
                                            ]);
        $chats = Chat::join('chat_users', 'chat_users.chat_id', '=', 'chats.id')
            ->where(
                    'chat_users.user_id', '=', $validatedData['id']
                )
            ->where(
                'chat_users.is_active', '=', 1
            )
            ->where(
                'chats.is_active', '=', 1
            )
            ->orderBy('created_at', 'desc')
            ->withCount(['messages'])
            ->get();

        return $chats->toJson();
    }

    public function show($id)
    {
        $chat = Chat::get()->find($id);
        $chat->messages = Message::getMessages($id);
        return $chat->toJson();
    }

    public function getChatUsers($id)
    {
        $chat_user = ChatUser::getUsers($id);
        return $chat_user->toJson();
    }

    public function sendMessage(Request $request)
    {

        $validatedData = $request->validate([
                                                'text' => 'required',
                                                'userId' => 'required',
                                                'chatId' => 'required'
                                            ]);

        $message = Message::create([
                                     'text' => $validatedData['text'],
                                     'user_id' => $validatedData['userId'],
                                     'chat_id' => $validatedData['chatId'],
                                 ]);
        broadcast(new MessageSent( ))->toOthers();

        return $message->toJson();
    }
}
