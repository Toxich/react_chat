<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['chat_id', 'user_id', 'text'];

    public static function getMessages($id)
    {
        $messages = Message::join('users', 'messages.user_id', '=', 'users.id')
            ->select('messages.id', 'text', 'users.name', 'messages.created_at', 'chat_id', 'user_id')
            ->where(
                'messages.chat_id', '=', $id
            )
            ->get();
        return $messages;
    }
}
