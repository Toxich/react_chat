<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatUser extends Model
{
    public static function getUsers($id)
    {
        $users = ChatUser::join('users', 'chat_users.user_id', '=', 'users.id')
            ->where(
                'chat_users.chat_id', '=', $id
            )
            ->where(
                'chat_users.is_active', '=', 1
            )
            ->get();
        return $users;
    }
}
