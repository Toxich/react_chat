import { Link } from 'react-router-dom'
import React, { Component } from 'react'

class Header extends Component {
    logoutUser = () => {
        let appState = {
            isLoggedIn: false,
            user: {}
        };
        // save app state with user date in local storage
        localStorage["appState"] = JSON.stringify(appState);
        this.setState(appState);
        window.location.href = '/';
    };
    render ()
    {
        let state = JSON.parse(localStorage.appState);
        const isLoggedIn = state.isLoggedIn;

        let button, register;
        if (isLoggedIn) {
            button = <a href={"#"} className='navbar-brand' onClick={() => { this.logoutUser() }} >Logout</a>;
        } else {
            button = <Link className='navbar-brand' to='/login'>Login</Link>;
            register = <Link className='navbar-brand' to='/register'>Sign Up</Link>;
        }
        return (
            <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
                <div className='container col-md-10'>
                    <Link className='navbar-brand' to='/home'>Tasksman</Link>
                </div>
                <div className='container'>
                    {button}
                    {register}
                </div>
            </nav>
        )
    }
}

export default Header
