import {Link} from 'react-router-dom'
import React, {Component} from 'react'

class Messages extends Component {
    scrollToBottom = () => {
        var element = document.getElementById("messages");
        element.scrollTop = element.scrollHeight;
    };

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        var messagesList = this.props.messagesList;
        return (
            <div className="messages" id='messages'>
                <ul>
                    {messagesList.map(message => (
                        <li className={message.class} key={message.id}>
                            <img src="http://emilcarlsson.se/assets/mikeross.png" alt=""/>
                            <p>
                                <span className={"message-nick"}>{message.user_nickname}</span>
                                <br/>
                                {message.text}
                                <br/>
                                <span className={'message-details'}> ({message.time})</span>
                            </p>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default Messages
