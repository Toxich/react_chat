import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const AuthRoute = ({ component: Component, ...rest }) => {

    // Add your own authentication on the below line.
    let state = JSON.parse(localStorage.appState);
    const isLoggedIn = state.isLoggedIn;

    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn ? (
                    <Redirect to={{ pathname: '/', state: { from: props.location } }} />
                ) : (
                    <Component {...props} />
                )
            }
        />
    )
}

export default AuthRoute
