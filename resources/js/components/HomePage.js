import { Link } from 'react-router-dom'
import React, { Component } from 'react'

class Header extends Component {

    render ()
    {
        return (
            <section className="jumbotron text-center ">
                <div className="container">
                    <h1>Welcome to React Chat</h1>
                    <p className="lead text-muted">Log in or register to start chatting with your friends!</p>
                    <p>
                        <a href="/login" className="btn btn-primary my-2">Login</a>
                        <a href="/register" className="btn btn-secondary my-2">Register</a>
                    </p>
                </div>
            </section>

        )
    }

}


export default Header
