import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class ChatsList extends Component {
    constructor () {
        super()
        this.state = {
            chats: []
        }
    }

    componentDidMount () {
        let state = JSON.parse(localStorage.appState);
        const data = {
            id: state.user.id
        };
        axios.post('/api/chats', data).then(response => {
            this.setState({
                chats: response.data
            })
        })
    }

    render () {
        const { chats } = this.state;
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-8'>
                        <div className='card'>
                            <div className='card-header'>All chats</div>
                            <div className='card-body'>
                                <Link className='btn btn-primary btn-sm mb-3' to='/chats/create'>
                                    Create new chat
                                </Link>
                                <ul className='list-group list-group-flush'>
                                    {chats.map(chats => (
                                        <Link
                                            className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                                            to={`/${chats.id}`}
                                            key={chats.id}
                                        >
                                            {chats.name}
                                            <span className='badge badge-primary badge-pill'>
                            {chats.messages_count}
                          </span>
                                        </Link>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ChatsList
