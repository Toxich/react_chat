import React from "react";
import {Link} from "react-router-dom";

const Register = () => {
    let _email, _password, _name;

    const handleLogin = e => {
        if (_email.value === '' || _password.value === '' || _name.value === '') {
            alert("Enter all data");
        } else {
            var formData = new FormData();
            formData.append("password", _password.value);
            formData.append("email", _email.value);
            formData.append("name", _name.value);

            axios
                .post("/api/user/register", formData)
                .then(response => {
                    console.log(response);
                    return response;
                })
                .then(json => {
                    if (json.data.success) {

                        let userData = {
                            name: json.data.data.name,
                            id: json.data.data.id,
                            email: json.data.data.email,
                            auth_token: json.data.data.auth_token,
                            timestamp: new Date().toString()
                        };
                        let appState = {
                            isLoggedIn: true,
                            user: userData
                        };
                        // save app state with user date in local storage
                        localStorage["appState"] = JSON.stringify(appState);
                        window.location.href = '/';
                    } else {
                        alert(`Registration Failed!`);
                        $("#email-login-btn")
                            .removeAttr("disabled")
                            .html("Register");

                    }
                })
                .catch(error => {
                    alert("An Error Occured!" + error);
                    console.log(`${formData} ${error}`);
                    $("#email-login-btn")
                        .removeAttr("disabled")
                        .html("Register");
                });
        }
        e.preventDefault();
        //  registerUser(_name.value, _email.value, _password.value, e);
    };
    return (
        <div id="main">
            <form id="login-form" className={'text-center p-5'} action="" onSubmit={handleLogin} method="post">
                <h3 style={{padding: 15}}>Register Form</h3>
                <div className={'d-flex justify-content-center'}>
                    <div className={' col-md-6'}>
                        <input ref={input => (_name = input)} autoComplete="off" id="email-input" name="email"
                               type="text"
                               className="form-control mb-4 center-block" placeholder="Name"/>
                        <input ref={input => (_email = input)} autoComplete="off" id="email-input" name="email"
                               type="text"
                               className="form-control mb-4 center-block" placeholder="email"/>
                        <input ref={input => (_password = input)} autoComplete="off" id="password-input" name="password"
                               type="password" className="form-control mb-4  center-block" placeholder="password"/>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary mr-4 center-block text-center" id="email-login-btn">
                    Register
                </button>

                <Link  to="/login">
                    Login
                </Link>
            </form>
        </div>
    );
};

export default Register;
