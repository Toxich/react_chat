import {Link} from 'react-router-dom'
import React, {Component} from 'react'
import Messages from './Messages'
import axios from "axios";

let elem;
let chatMessages;

Echo.channel('chat')
    .listen('MessageSent', (e) => {
        const chatId = elem.props.match.params.id;
        axios.get(`/api/chats/${chatId}`).then(response => {
            let messages = response.data.messages;
            elem.setState({messages: messages});
        })
    });

class Chat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chat: {},
            users: [],
            messages: [],
            title: '',
            errors: [],
            userId: 0,
            inputValue: '',
        };
        elem = this;
    }

    hasErrorFor(field) {
        return !!this.state.errors[field]
    }

    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
            <strong>{this.state.errors[field][0]}</strong>
          </span>
            )
        }
    }

    sendMessage = () => {
        const {
            chat,
            messages,
            userId,
            inputValue
        } = this.state;
        if (inputValue) {
            let messageValue = {
                text: inputValue,
                userId: userId,
                chatId: chat.id,
            };
            axios.post('api/chats/send-message', messageValue)
                .then(response => {
                    const chatId = elem.props.match.params.id;
                    axios.get(`/api/chats/${chatId}`).then(response => {
                        let messages = response.data.messages;
                        elem.setState({messages: messages, inputValue: ''});
                    })
                })
                .catch(error => {
                    if (error.response) {
                        this.setState({
                            errors: error.response.data.errors
                        })
                    }
                });
        }
    };

    onChange = (e) => this.setState({inputValue: e.target.value});

    componentDidMount() {
        const chatId = this.props.match.params.id;
        let state = JSON.parse(localStorage.appState);
        const userId = state.user.id;
        axios.get(`/api/chats/${chatId}`).then(response => {
            this.setState({
                chat: response.data,
                messages: response.data.messages,
                userId: userId
            });
            axios.get(`/api/chats/users/${chatId}`).then(response => {
                this.setState({
                    users: response.data,
                });
            })
        });
    }

    render() {

        const {chat, messages, users} = this.state;
        let messagesList = [];
        let users_list = [];
        var userId = this.state.userId;
        users.map(function (user, i) {
            users_list.push(
                {
                    name: user.name,
                    id: user.id
                }
            )
        });
        messages.map(function (message, i) {
                let date = new Date(message.created_at);
                let sent_time = date.getHours() + ":" + date.getMinutes();
                if (message.user_id === userId) {
                    messagesList.push(
                        {
                            class: 'replies',
                            text: message.text,
                            id: message.id,
                            time: sent_time,
                            user_nickname: message.name
                        }
                    );
                } else {
                    messagesList.push(
                        {
                            class: 'sent',
                            text: message.text,
                            id: message.id,
                            time: sent_time,
                            user_nickname: message.name
                        }
                    );
                }
            }
        );

        return (
            <div id="frame" className={'mt-5'}>
                <div className={"list"}>
                    <h4>Users:</h4>
                    <ul>
                        {users_list.map(user => (
                            <li key={user.id} >
                                <p>
                                    {user.name}
                                </p>
                            </li>
                        ))}
                    </ul>
                </div>
                <div className="content">
                    <div className="contact-profile">
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt=""/>
                        <p>{chat.name}</p>
                    </div>
                    <Messages messagesList={messagesList}/>
                    <div className="message-input">
                        <div className="wrap">
                            <input type="text" value={elem.state.inputValue} onChange={this.onChange}
                                   placeholder="Write your message..."/>
                            <button className="submit" onClick={() => {
                                this.sendMessage()
                            }}>
                                Send
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Chat;
