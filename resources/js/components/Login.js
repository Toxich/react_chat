import React from "react";
import {Link} from "react-router-dom";

const Login = ({history, loginUser = f => f}) => {
    let _email, _password;
    const handleLogin = e => {
        $("#login-form button")
            .attr("disabled", "disabled")
        var formData = new FormData();
        formData.append("email", _email.value);
        formData.append("password", _password.value);

        axios
            .post("/api/user/login/", formData)
            .then(response => {
                return response;
            })
            .then(json => {
                if (json.data.success) {
                    let userData = {
                        name: json.data.data.name,
                        id: json.data.data.id,
                        email: json.data.data.email,
                        auth_token: json.data.data.auth_token,
                        timestamp: new Date().toString()
                    };
                    let appState = {
                        isLoggedIn: true,
                        user: userData
                    };
                    // save app state with user date in local storage
                    localStorage["appState"] = JSON.stringify(appState);
                    window.location.href = '/';
                } else alert("Login Failed!");

                $("#login-form button")
                    .removeAttr("disabled")
                    .html("Login");
            })
            .catch(error => {
                alert(`An Error Occured! ${error}`);
                $("#login-form button")
                    .removeAttr("disabled")
                    .html("Login");
            });
        e.preventDefault();
    };
    return (
        <div id="main">
            <form id="login-form" className={'text-center p-5'} action="" onSubmit={handleLogin} method="post">
                <h3 style={{padding: 15}}>Login Form</h3>
                <div className={'d-flex justify-content-center'}>
                    <div className={' col-md-6'}>
                        <input ref={input => (_email = input)} autoComplete="off" id="email-input" name="email"
                               type="text" className=" form-control mb-4 center-block " placeholder="email"/>
                        <input ref={input => (_password = input)} autoComplete="off" id="password-input" name="password"
                               type="password" className="form-control mb-4 center-block" placeholder="password"/>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary mr-4 center-block text-center" id="email-login-btn"
                        href="#facebook">
                    Login
                </button>
                <Link to="/register">
                    Register
                </Link>
            </form>

        </div>
    );
};

export default Login;
