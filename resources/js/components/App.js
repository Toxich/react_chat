import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import NewProject from './NewProject'
import ProjectsList from './ProjectsList'
import SingleProject from './SingleProject'
import Login from './Login'
import Register from './Register'
import Chat from './Chat'
import ChatsList from './ChatsList'
import HomePage from './HomePage'
import PrivateRoute from "./PrivateRoute";
import AuthRoute from "./AuthRoute";
import ChatCreate from "./ChatCreate";

class App extends Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: false,
            user: {}
        };
    }


    componentDidMount() {
        let state = localStorage["appState"];
        console.log(state);
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState });
        }
    }
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        {/*<Route exact path='/' component={ProjectsList} />*/}
                        <PrivateRoute exact path="/" component={ChatsList} />
                        <PrivateRoute exact path="/chats/create" component={ChatCreate} />
                        <AuthRoute exact path='/login' component={Login} />
                        <AuthRoute exact path='/home' component={HomePage} />
                        <AuthRoute exact path='/register' component={Register} />
                        <PrivateRoute path='/:id' component={Chat} />
                        <PrivateRoute path='/create' component={NewProject} />
                        <PrivateRoute path='/projects/:projId' component={SingleProject} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))
